const path = require('path')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const MiniCssExtractPlugin = require('mini-css-extract-plugin')

module.exports = function (env, options) {
  return {
    mode: 'production',
    entry: './src/js/app.js',
    output: {
      path: path.join(__dirname, 'dist'),
      filename: '[name].[hash].js'
    },
    plugins: [
      new HtmlWebpackPlugin({
        template: 'src/index.html'
      }),
      new MiniCssExtractPlugin({
        filename: '[name].[hash].css'
      })
    ],
    module: {
      rules: [
        {
          test: /\.js$/,
          exclude: /(node_modules|bower_components)/,
          use: {
            loader: 'babel-loader',
            options: {
              presets: ['@babel/preset-env']
            }
          }
        },
        {
          test: /\.html/,
          use: 'html-loader'
        },
        {
          test: /\.(sa|sc|c)ss$/,
          use: [
            options.mode === 'development' ? 'style-loader' : MiniCssExtractPlugin.loader,
            'css-loader',
            'postcss-loader',
            'sass-loader',
          ]
        },
        {
          test: /\.(png|svg|jpg|gif)$/,
          use: [
            {
              loader: 'file-loader',
              options: {
                name: 'img/[name]-[hash].[ext]',
              }
            }
          ],
        }
      ]
    },
    devServer: {
      contentBase: path.resolve(__dirname, 'dist'),
      host: '0.0.0.0'
    }
  }
}