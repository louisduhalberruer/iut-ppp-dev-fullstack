import '../scss/app.scss'

import _ from 'lodash'

document.body.onload = function () {
  document.querySelectorAll('[data-animate]').forEach(function (entry) {
    entry.animated = true
  })

  window.onscroll = _.throttle(function () {
    updateAnimate()
  }, 100)

  function updateAnimate() {
    document.querySelectorAll('[data-animate]').forEach(function (entry) {
      let rect = entry.getBoundingClientRect()
      if (rect.top < window.innerHeight / 1.5 && !entry.animated) {
        entry.style.opacity = '1'
        entry.className += ' ' + entry.dataset.animate
        entry.animated = true
      } else if (rect.top > window.innerHeight && entry.animated) {
        entry.style.opacity = '0'
        entry.className = entry.className.replace(' ' + entry.dataset.animate, '')
        entry.animated = false
      }
    })
  }

  updateAnimate()
}